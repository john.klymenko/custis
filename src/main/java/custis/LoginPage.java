package custis;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {

    @FindBy(name = "username")
    private WebElement loginField;

    @FindBy(xpath = "//input[@type='password']")
    private WebElement passwordField;

    @FindBy(className = "kc-feedback-text")
    private WebElement validationMessageBlock;

    @FindBy(id = "kc-login")
    private WebElement loginButton;

    private WebDriver driver;

    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public LoginPage enterPassword(String password) {
        passwordField.sendKeys(password);
        return this;
    }

    public LoginPage enterLogin(String login) {
        loginField.sendKeys(login);
        return this;
    }

    public LoginPage checkValidationMessage() {
        validationMessageBlock.isDisplayed();
        String actualValidationMessage = validationMessageBlock.getText();
        String expectedValidationMessage = "Invalid username or password. blablabla";
        Assert.assertEquals(expectedValidationMessage, actualValidationMessage);
        return this;
    }

    public LoginPage pressLoginButton() {
        loginButton.click();
        return this;
    }

    public LoginPage openLoginPage(String url) {
        driver.get(url);
        return this;
    }
}
