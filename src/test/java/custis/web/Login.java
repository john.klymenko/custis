package custis.web;

import custis.LoginPage;
import org.junit.Test;

public class Login extends AbstractSeleniumTest {

    @Test
    public void loginWithoutCredentials() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage
                .openLoginPage(custisStagingUrl)
                .pressLoginButton()
                .checkValidationMessage();
    }

    @Test
    public void tryToLoginWithCorrectLoginAndEmptyPassword() {
        LoginPage loginPage = new LoginPage(driver);
        loginPage.openLoginPage(custisStagingUrl)
                .enterLogin("autotest@demo")
                .pressLoginButton()
                .checkValidationMessage();
    }

}