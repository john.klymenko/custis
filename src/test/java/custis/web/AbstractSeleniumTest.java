package custis.web;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public abstract class AbstractSeleniumTest {

    protected WebDriver driver;
    protected String custisStagingUrl;

    @Before
    public void setUp() {
        System.setProperty("webdriver.chrome.driver", "chromedriver.exe");
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--lang=en");
        driver = new ChromeDriver(options);
        custisStagingUrl = "https://tms-autotest.custis.ru:443";
    }

    @After
    public void tearDown() {
        driver.quit();
    }
}